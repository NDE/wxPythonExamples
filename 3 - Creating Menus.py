#!/usr/bin/env python
import wx


class MyFrame(wx.Frame):
    """ We simply derive a new class of Frame. """
    def __init__(self, parent, title):
        # Call parent initializer
        # size=(-1,-1) represents default size
        wx.Frame.__init__(self, parent, title=title, size=(-1, -1))

        self.create_menus()

        # Show the frame.
        self.Show(True)

    def create_menus(self):
        # A Statusbar in the bottom of the window
        self.CreateStatusBar()

        # Setting up the menu.
        file_menu = wx.Menu()

        # wx.ID_ABOUT, wx.ID_EXIT, .. are standard IDs provided by wxWidgets.
        # the '&' lets wxPython create a 'ctrl' keyboard shortcut with the following letter
        self.new_menu = wx.MenuItem(file_menu, wx.ID_NEW, "&New", " Clear GUI state to default")
        file_menu.AppendItem(self.new_menu)

        self.open_menu = wx.MenuItem(file_menu, wx.ID_OPEN, "&Open", " Open a saved GUI state")
        file_menu.AppendItem(self.open_menu)

        self.save_menu = wx.MenuItem(file_menu, wx.ID_SAVE, "&Save", " Save state of GUI")
        file_menu.AppendItem(self.save_menu)

        self.exit_menu = wx.MenuItem(file_menu, wx.ID_EXIT, "E&xit", " Terminate the program")
        file_menu.AppendItem(self.exit_menu)

        # Create another menu for info
        info_menu = wx.Menu()
        self.about_menu = wx.MenuItem(info_menu, wx.ID_ABOUT, "&About", " Information about this program")
        info_menu.AppendItem(self.about_menu)

        # Creating the menubar and add the menus
        menu_bar = wx.MenuBar()
        menu_bar.Append(file_menu, "&File")
        menu_bar.Append(info_menu, "&Info")

        # Adding the MenuBar to the Frame content.
        self.SetMenuBar(menu_bar)


# Create a new app, don't redirect stdout/stderr to a window.
app = wx.App(False)

# Instantiate the MyFrame Class
frame = MyFrame(None, 'Hello World')

# Start Event Loop
app.MainLoop()
