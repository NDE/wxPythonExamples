#!/usr/bin/env python
import wx


class MyFrame(wx.Frame):
    """ We simply derive a new class of Frame. """
    def __init__(self, parent, title):
        # Call parent initializer
        # size=(-1,-1) represents default size
        wx.Frame.__init__(self, parent, title=title, size=(-1, -1))

        # Show the frame.
        self.Show(True)

# Create a new app, don't redirect stdout/stderr to a window.
app = wx.App(False)

# Instantiate the MyFrame Class
frame = MyFrame(None, 'Hello World')

# Start Event Loop
app.MainLoop()
