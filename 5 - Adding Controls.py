#!/usr/bin/env python
import wx


class MyFrame(wx.Frame):
    """ We simply derive a new class of Frame. """
    def __init__(self, parent, title):
        # Call parent initializer
        # size=(-1,-1) represents default size
        wx.Frame.__init__(self, parent, title=title, size=(-1, -1))

        # Add a Panel to the frame
        self.panel = MyPanel(self)

        self.create_menus()

        # Show the frame.
        self.Show(True)

    def create_menus(self):
        # A Statusbar in the bottom of the window
        self.CreateStatusBar()

        # Setting up the menu.
        file_menu = wx.Menu()

        # wx.ID_ABOUT, wx.ID_EXIT, .. are standard IDs provided by wxWidgets.
        # the '&' lets wxPython create a 'ctrl' keyboard shortcut with the following letter
        self.new_menu = wx.MenuItem(file_menu, wx.ID_NEW, "&New", " Clear GUI state to default")
        file_menu.AppendItem(self.new_menu)

        self.open_menu = wx.MenuItem(file_menu, wx.ID_OPEN, "&Open", " Open a saved GUI state")
        file_menu.AppendItem(self.open_menu)

        self.save_menu = wx.MenuItem(file_menu, wx.ID_SAVE, "&Save", " Save state of GUI")
        file_menu.AppendItem(self.save_menu)

        self.exit_menu = wx.MenuItem(file_menu, wx.ID_EXIT, "E&xit", " Terminate the program")
        file_menu.AppendItem(self.exit_menu)

        # Create another menu for info
        info_menu = wx.Menu()
        self.about_menu = wx.MenuItem(info_menu, wx.ID_ABOUT, "&About", " Information about this program")
        info_menu.AppendItem(self.about_menu)

        # Creating the menubar and add the menus
        menu_bar = wx.MenuBar()
        menu_bar.Append(file_menu, "&File")
        menu_bar.Append(info_menu, "&Info")

        # Adding the MenuBar to the Frame content.
        self.SetMenuBar(menu_bar)


class MyPanel(wx.Panel):
    def __init__(self, parent):
        # Call parent initializer
        wx.Panel.__init__(self, parent=parent)
        # Create a sizer
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)

        self.add_controls()

        #  Set the sizer to the panel
        self.SetSizer(self.mainSizer)

    def add_controls(self):
        # Instantiate controls
        self.slope_text_control = wx.TextCtrl(self, -1)
        self.y_intercept_combobox = wx.ComboBox(self, -1, choices=['-3','-2','-1','0','1','2','3'])
        self.graph_color_choice = wx.Choice(self, -1, choices=['red', 'blue', 'green'])
        self.graph_button = wx.Button(self, -1, label="Graph!")

        # Create gridBagSizer with 10 vertical and horizontal spacing
        gbs = self.gbs = wx.GridBagSizer(vgap=10, hgap=10)

        # Add controls to sizer specifiying grid position
        gbs.Add( wx.StaticText(self, -1, "Slope"),(0,0))
        gbs.Add( self.slope_text_control,(0,1))

        gbs.Add( wx.StaticText(self, -1, "Y intercept"),(1,0))
        gbs.Add( self.y_intercept_combobox,(1,1), flag=wx.EXPAND)

        gbs.Add( wx.StaticText(self, -1, "Graph Color"),(2,0))
        gbs.Add( self.graph_color_choice,(2,1),flag=wx.EXPAND)

        # give a larger spand and expand field
        gbs.Add( self.graph_button,(3,0), span=(2,2),flag=wx.EXPAND )

        # Add the GridBagSizer to the mainsizer
        self.mainSizer.Add(gbs, 1, wx.ALL|wx.EXPAND, 10)


# Create a new apself, don't redirect stdout/stderr to a window.
app = wx.App(False)

# Instantiate the MyFrame Class
frame = MyFrame(None, 'Graphing')

# Start Event Loop
app.MainLoop()
