#!/usr/bin/env python
import wx
import os
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure
import numpy as np

class MyFrame(wx.Frame):
    """ We simply derive a new class of Frame. """
    def __init__(self, parent, title):
        # Call parent initializer
        # size=(-1,-1) represents default size
        wx.Frame.__init__(self, parent, title=title, size=(-1, -1))

        # Add a Panel to the frame
        self.panel = MyPanel(self)

        self.create_menus()
        self.connect_menus()

        # Show the frame.
        self.Show(True)

    def create_menus(self):
        # A Statusbar in the bottom of the window
        self.CreateStatusBar()

        # Setting up the menu.
        file_menu = wx.Menu()

        # wx.ID_ABOUT, wx.ID_EXIT, .. are standard IDs provided by wxWidgets.
        # the '&' lets wxPython create a 'ctrl' keyboard shortcut with the following letter
        self.new_menu = wx.MenuItem(file_menu, wx.ID_NEW, "&New", " Clear GUI state to default")
        file_menu.AppendItem(self.new_menu)

        self.open_menu = wx.MenuItem(file_menu, wx.ID_OPEN, "&Open", " Open a saved GUI state")
        file_menu.AppendItem(self.open_menu)

        self.save_menu = wx.MenuItem(file_menu, wx.ID_SAVE, "&Save", " Save state of GUI")
        file_menu.AppendItem(self.save_menu)

        self.exit_menu = wx.MenuItem(file_menu, wx.ID_EXIT, "E&xit", " Terminate the program")
        file_menu.AppendItem(self.exit_menu)

        # Create another menu for info
        info_menu = wx.Menu()
        self.about_menu = wx.MenuItem(info_menu, wx.ID_ABOUT, "&About", " Information about this program")
        info_menu.AppendItem(self.about_menu)

        # Creating the menubar and add the menus
        menu_bar = wx.MenuBar()
        menu_bar.Append(file_menu, "&File")
        menu_bar.Append(info_menu, "&Info")

        # Adding the MenuBar to the Frame content.
        self.SetMenuBar(menu_bar)

    def connect_menus(self):
        self.Bind(wx.EVT_MENU,self.open, self.open_menu)
        self.Bind(wx.EVT_MENU,self.save, self.save_menu)
        self.Bind(wx.EVT_MENU,self.exit, self.exit_menu)
        self.Bind(wx.EVT_MENU,self.about, self.about_menu)
        self.Bind(wx.EVT_MENU,self.new, self.new_menu)

    def save(self, event):
        dlg = wx.FileDialog(self, "Save file as...", os.getcwd(), style=wx.SAVE | wx.OVERWRITE_PROMPT,
                            wildcard="Text files (*.txt)|*.txt|All files (*.*)|*.*")
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()
        dlg.Destroy()
        return

    def open(self, event):
        # give dialog to find the file
        dlg = wx.FileDialog(self, "Open file...", os.getcwd(), style=wx.OPEN,
                            wildcard="Text files (*.txt)|*.txt|All files (*.*)|*.*")

        # if user selects okay on the dialog
        if dlg.ShowModal() == wx.ID_OK:
            # save the pathname from the dialog
            filename = dlg.GetPath()
        dlg.Destroy()

    def about(self, event):
            dlg = wx.MessageDialog(self,'This is a pop up dialog','About this Program', wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()

    def new(self, event):
        self.panel.graph_color_choice.SetLabel('')
        self.panel.y_intercept_combobox.SetLabel('')
        self.panel.slope_text_control.Clear()

    def exit(self, event):
        self.Destroy()


class MyPanel(wx.Panel):
    def __init__(self, parent):
        # Call parent initializer
        wx.Panel.__init__(self, parent=parent)
        # Create a sizer
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)

        self.add_controls()

        #  Set the sizer to the panel
        self.SetSizer(self.mainSizer)

        # Connect the control event to an event handler
        self.bind_controls()

    def add_controls(self):
        # Instantiate controls
        self.slope_text_control = wx.TextCtrl(self, -1)
        self.y_intercept_combobox = wx.ComboBox(self, -1, choices=['-3','-2','-1','0','1','2','3'])
        self.graph_color_choice = wx.Choice(self, -1, choices=['red', 'blue', 'green'])
        self.graph_button = wx.Button(self, -1, label="Graph!")

        # Create gridBagSizer with 10 vertical and horizontal spacing
        gbs = self.gbs = wx.GridBagSizer(vgap=10, hgap=10)

        # Add controls to sizer specifiying grid position
        gbs.Add( wx.StaticText(self, -1, "Slope"),(0,0))
        gbs.Add( self.slope_text_control,(0,1))

        gbs.Add( wx.StaticText(self, -1, "Y intercept"),(1,0))
        gbs.Add( self.y_intercept_combobox,(1,1), flag=wx.EXPAND)

        gbs.Add( wx.StaticText(self, -1, "Graph Color"),(2,0))
        gbs.Add( self.graph_color_choice,(2,1),flag=wx.EXPAND)

        # give a larger spand and expand field
        gbs.Add( self.graph_button,(3,0), span=(2,2),flag=wx.EXPAND )

        # Add the GridBagSizer to the mainsizer
        self.mainSizer.Add(gbs, 1, wx.ALL|wx.EXPAND, 10)

    def bind_controls(self):
        self.slope_text_control.Bind(wx.EVT_TEXT, self.slope_test_control_handler)
        # Need to handle both selecting a value and manually entering a value
        self.y_intercept_combobox.Bind(wx.EVT_COMBOBOX, self.y_intercept_combobox_handler)
        self.y_intercept_combobox.Bind(wx.EVT_TEXT, self.y_intercept_combobox_handler)
        self.graph_color_choice.Bind(wx.EVT_CHOICE, self.graph_color_choice_handler)
        self.graph_button.Bind(wx.EVT_BUTTON, self.graph_button_handler)

    def slope_test_control_handler(self, event):
        print self.slope_text_control.GetValue()

    def y_intercept_combobox_handler(self, event):
        print self.y_intercept_combobox.GetValue()

    def graph_color_choice_handler(self, event):
        print self.graph_color_choice.GetString(self.graph_color_choice.GetSelection())

    def graph_button_handler(self, event):
        slope = float(self.slope_text_control.GetValue())
        y_intercept = float(self.y_intercept_combobox.GetValue())
        color = self.graph_color_choice.GetString(self.graph_color_choice.GetSelection())
        graph_frame = GraphFrame(slope, y_intercept, color)
        graph_frame.Show(True)


class GraphFrame(wx.Frame):
    def __init__(self, slope, y_intercept, color):
        wx.Frame.__init__(self, None, -1, "Graph")
        self.initialize_plots(slope, y_intercept, color)
        self.initialize_toolbar()

    def initialize_plots(self, slope, y_intercept, color):
        self.SetBackgroundColour(wx.NamedColour("WHITE"))

        # set up subplots
        self.figure = Figure()

        graph = self.figure.add_subplot(111)
        x,y = self.f(slope, y_intercept)
        graph.plot(x, y, color)

        graph.set_xlabel('my xdata')
        graph.set_ylabel('my ydata')

        # canvas and sizing details
        self.figure_canvas = FigureCanvas(self, -1, self.figure)

        # Create the handler for clicking on the depth profile
        #self.figure_canvas.mpl_connect('button_press_event', self.update_graph)

        # figure sizing stuff
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.figure_canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.SetSizer(self.sizer)
        self.Fit()
        self.figure_canvas.draw()

    def initialize_toolbar(self):
        # toolbar which allows users to save, zoom, pan, ect...
        self.statusBar = wx.StatusBar(self, -1)
        self.statusBar.SetFieldsCount(1)
        self.SetStatusBar(self.statusBar)

        # add toolbar to allow user to zoom, save, ect
        self.toolbar = NavigationToolbar2Wx(self.figure_canvas)
        self.sizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        self.toolbar.Show()

    def f(self, slope, y_int):
        x = np.array(range(-10, 10))
        y = slope*x + y_int
        return x,y


# Create a new apself, don't redirect stdout/stderr to a window.
app = wx.App(False)

# Instantiate the MyFrame Class
frame = MyFrame(None, 'Graphing')

# Start Event Loop
app.MainLoop()
